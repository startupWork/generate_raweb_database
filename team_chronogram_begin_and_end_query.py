import logging, configparser
from pymongo import MongoClient
# Config properties
config = configparser.RawConfigParser()
config.read("ConfigFile.properties")

# Logger configuration
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__.split('/')[-1])

mongodb_ip = config.get("mongodb", "ip")
mongodb_port = int(config.get("mongodb", "port"))
mongodb_db = config.get("mongodb", "database")

# MongoDB client
client = MongoClient(mongodb_ip, mongodb_port)

if __name__ == '__main__':

    # Find team begin and end based on raweb dataset
    db = client.get_database(mongodb_db)
    teams_chronogram = db.teams_chronogram.find()
    for team_chronogram in teams_chronogram:
        acronym = team_chronogram["acronym"]
        all_years = [x["year"] for x in team_chronogram["chronogram"]]
        print(acronym, min(all_years), max(all_years))
