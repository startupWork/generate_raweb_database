import logging, configparser
from pymongo import MongoClient
from bs4 import BeautifulSoup
import requests
import re

# Config properties
config = configparser.RawConfigParser()
config.read("ConfigFile.properties")

# Logger configuration
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__file__.split('/')[-1])

mongodb_ip = config.get("mongodb", "ip")
mongodb_port = int(config.get("mongodb", "port"))
mongodb_db = config.get("mongodb", "database")

# MongoDB client
client = MongoClient(mongodb_ip, mongodb_port)


def database_init():
    # Delete database if it exists
    client.drop_database(mongodb_db)
    client.close()


def data_to_mongodb(json):
    client = MongoClient(mongodb_ip, mongodb_port)
    db = client.get_database(mongodb_db)
    try:
        db.teams.insert(json)
        client.close()
    except BaseException as e:
        logger.warning(str(e))
        pass


def get_1994_team_activities_raweb():
    teams = dict()
    year = 1994
    link = "http://raweb.inria.fr/rapportsactivite/RA1994/RA94.liste.html"
    response = requests.get(link)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("li")
    if bool(results):
        list_teams = []
        for result in results:
            team = dict()
            team["acronym"] = result.a.text
            team["url"] = result.a['href']
            team["year"] = str(year)
            list_teams.append(team)
        teams[str(year)] = list_teams
    return teams


def get_1994_team_composition(link_report_activity, acronymn, year):
    team_structure = dict()
    team_members = []
    categories = []
    print(link_report_activity)
    link_to_find = "Composition de l'équipe"
    link_team_composition = ""
    transform_link_report_activity = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    tokens = transform_link_report_activity.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"

    response2 = requests.get(link_report_activity)
    soup2 = BeautifulSoup(response2.content, "html.parser")

    results2 = soup2.findAll("h3")
    # print(results2)
    if results2:
        for result2 in results2:
            new_result2 = " ".join(result2.text.replace('\n', ' ').strip().split())
            # print(new_result2)
            # print(result2)
            if link_to_find in new_result2:
                link_team_composition = result2.a['href']
                break
    link_to_parse = new_link_start + "/" + link_team_composition
    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    results3 = soup.findAll("h2")
    if results3:
        for result3 in results3:
            cat = " ".join(result3.text.replace('\n', ' ').strip().split())
            if cat != "" and cat not in categories:
                categories.append(" ".join(cat.split()))

    results = soup.findAll(["h2", "li"])
    assign_category = ""
    for result in results:
        elem = " ".join(result.text.replace('\n', ' ').strip().split())
        if elem in categories:
            assign_category = elem
        else:
            all_infos = result.text
            more_infos = ""
            all_infos_split = all_infos.split(",")
            firstname_lastname = all_infos_split[0].strip()
            for i in range(1, len(all_infos_split)):
                more_infos = more_infos + all_infos_split[i].strip() + " "
            moreinfos = " ".join(more_infos.replace('\n', ' ').strip().split())
            member = dict()
            tokens = firstname_lastname.split(' ')
            firstname = tokens[0].strip()
            lastname = firstname_lastname[len(firstname) + 1:].strip()
            member["key"] = ""
            member["category"] = assign_category.lower()
            member["firstname"] = firstname
            member["lastname"] = lastname
            member["moreinfos"] = moreinfos
            team_members.append(member)

    team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def get_1996_to_1998_team_activities_raweb():
    teams = dict()
    urls = {"1996": "http://raweb.inria.fr/rapportsactivite/RA1996/liste.html",
            "1997": "http://raweb.inria.fr/rapportsactivite/RA1997/Rapports.html",
            "1998": "http://raweb.inria.fr/rapportsactivite/RA1998/Rapports.html"}

    for key in urls.keys():
        print(key)
        list_teams = []
        year = key
        link = urls.get(key)
        transform_link = link.replace("http://", "")
        new_link_start = "http://"
        tokens = transform_link.split("/")
        for val in tokens:
            if "html" not in val:
                new_link_start = new_link_start + val.strip() + "/"
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        results = soup.findAll("li")
        if bool(results):
            result_count = len(results)
            for j in range(4, result_count):
                result = results[j]
                team = dict()
                if result.a:
                    team = dict()
                    team["acronym"] = result.a.text
                    team["url"] = new_link_start + result.a['href']
                    team["year"] = key
                    print(result.a.text + ":  " + new_link_start + result.a['href'])
                    list_teams.append(team)
        teams[str(year)] = list_teams
    return teams


def get_1996_to_1998_team_composition(link_report_activity, acronym, year):
    if str(year) == "1998":
        return get_1988_team_composition(link_report_activity, acronym, year)

    team_structure = dict()
    team_members = []
    categories = []
    link_to_find = "Composition de l'équipe"
    to_replace = ["de l'Institut Elie Cartan", "extérieurs"]
    to_exclude = ["Précédent :", "Remonter :", "Suivant :"]
    url_start = "http://"
    end_url = ""
    new_link_start = ""
    transform_link = link_report_activity.replace("http://", "")
    tokens = transform_link.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val.strip() + "/"
    response = requests.get(link_report_activity)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("a")
    new_link_start = url_start + new_link_start
    if bool(results):
        for result in results:
            if result.text.strip() == link_to_find:
                # print(result.text.strip())
                end_url = result['href']
                break
    new_link_start = new_link_start + end_url
    print(new_link_start)
    response2 = requests.get(new_link_start)
    soup2 = BeautifulSoup(response2.content, "html.parser")
    results2 = soup2.findAll("b")
    if bool(results2):
        for result2 in results2:
            cat = " ".join(result2.text.replace('\n', ' ').strip().split())
            if cat not in to_exclude:
                print(cat)
                if cat != "" and cat not in categories:
                    categories.append(" ".join(cat.split()))

    response3 = requests.get(new_link_start)
    soup3 = BeautifulSoup(response3.content, "html.parser")
    results3 = soup3.findAll("body")
    if bool(results3):
        for result3 in results3:
            text = result3.text.strip()
            new_text = " ".join(text.split())
            for replace in to_replace:
                new_text = new_text.replace(replace, "")
            counter = 0
            categories_mappings = dict()
            for val1 in categories:
                for val2 in categories:
                    status = 0
                    if val1 != val2 and val1 in val2:
                        categories_mappings[val2] = "#" + str(counter) + "#"
                        break
                counter += 1

            keys = categories_mappings.keys()
            for i, n in enumerate(categories):
                if n in keys:
                    categories[i] = categories_mappings.get(n)
            clear_categories_mappings = dict()
            for val in categories:
                if "#" not in val:
                    clear_categories_mappings[val] = val
                else:
                    for key in keys:
                        if categories_mappings.get(key) == val:
                            clear_categories_mappings[key] = val

            for key in clear_categories_mappings.keys():
                if key in new_text:
                    replacement = clear_categories_mappings.get(key) + " "
                    new_text = new_text.replace(key, replacement)
            iter = len(categories) - 1
            position = 0

            while position < iter:

                begin = new_text.find(categories[position])
                end = new_text.find(categories[position + 1])

                extract_text = new_text[begin:end]

                extract_text = extract_text.replace(categories[position], "")
                result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
                for i in result:
                    member = dict()
                    category = categories[position]
                    for key in clear_categories_mappings.keys():
                        if category == clear_categories_mappings.get(key):
                            category = key
                            break
                    firstname_lastname = i.__getitem__(0).strip()
                    moreinfos = i.__getitem__(1)
                    tokens = firstname_lastname.split(' ')
                    firstname = tokens[0].strip()
                    lastname = firstname_lastname[len(firstname) + 1:].strip()
                    member["key"] = ""
                    member["category"] = category.strip().lower()
                    member["firstname"] = firstname
                    member["lastname"] = lastname
                    member["moreinfos"] = moreinfos.strip()
                    team_members.append(member)
                position += 1
            extract_text = new_text[new_text.find(categories[position]):]
            extract_text = extract_text.replace(categories[position], "")
            result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
            for i in result:
                member = dict()
                category = categories[position]
                for key in clear_categories_mappings.keys():
                    if category == clear_categories_mappings.get(key):
                        category = key
                        break
                firstname_lastname = i.__getitem__(0).strip()
                moreinfos = i.__getitem__(1)
                tokens = firstname_lastname.split(' ')
                firstname = tokens[0].strip()
                lastname = firstname_lastname[len(firstname) + 1:].strip()
                member["key"] = ""
                member["category"] = " ".join(category.replace('\n', ' ').strip().lower().split())
                member["firstname"] = firstname
                member["lastname"] = lastname
                member["moreinfos"] = moreinfos.strip()
                team_members.append(member)
        team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def get_1988_team_composition(link_report_activity, acronym, year):
    team_structure = dict()
    team_members = []
    categories = []
    link_to_find = "Composition de l'équipe"
    url_start = "http://"
    end_url = ""
    new_link_start = ""
    transform_link = link_report_activity.replace("http://", "")
    tokens = transform_link.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val.strip() + "/"
    response = requests.get(link_report_activity)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("a")
    new_link_start = url_start + new_link_start
    if bool(results):
        for result in results:
            if result.text.strip() == link_to_find:
                # print(result.text.strip())
                end_url = result['href']
                break
    new_link_start = new_link_start + end_url
    print(new_link_start)
    # print(link_to_parse)

    response2 = requests.get(new_link_start)
    soup2 = BeautifulSoup(response2.content, "html.parser")
    results2 = soup2.findAll("h3")
    if results2:
        for result2 in results2:
            cat = result2.text.replace('\n', ' ').strip()
            cat = " ".join(cat.split())
            if cat.strip() != "" and cat != link_to_find and cat not in categories:
                categories.append(cat)

        response3 = requests.get(new_link_start)
        soup3 = BeautifulSoup(response3.content, "html.parser")
        results3 = soup3.findAll("body")
        if results3:
            for result3 in results3:
                new_text = result3.text.replace('\n', ' ').strip()
                new_text = " ".join(new_text.split())

                counter = 0
                categories_mappings = dict()
                for val1 in categories:
                    for val2 in categories:
                        status = 0
                        if val1 != val2 and val1 in val2:
                            categories_mappings[val2] = "#" + str(counter) + "#"
                            break
                    counter += 1

                keys = categories_mappings.keys()
                for i, n in enumerate(categories):
                    if n in keys:
                        categories[i] = categories_mappings.get(n)
                clear_categories_mappings = dict()
                for val in categories:
                    if "#" not in val:
                        clear_categories_mappings[val] = val
                    else:
                        for key in keys:
                            if categories_mappings.get(key) == val:
                                clear_categories_mappings[key] = val

                for key in clear_categories_mappings.keys():
                    if key in new_text:
                        replacement = clear_categories_mappings.get(key) + " "
                        new_text = new_text.replace(key, replacement)
                iter = len(categories) - 1
                position = 0

                while position < iter:

                    begin = new_text.find(categories[position])
                    end = new_text.find(categories[position + 1])

                    extract_text = new_text[begin:end]

                    extract_text = extract_text.replace(categories[position], "")
                    result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
                    for i in result:
                        member = dict()
                        category = categories[position]
                        for key in clear_categories_mappings.keys():
                            if category == clear_categories_mappings.get(key):
                                category = key
                                break

                        firstname_lastname = i.__getitem__(0).strip()
                        moreinfos = i.__getitem__(1)
                        tokens = firstname_lastname.split(' ')
                        firstname = tokens[0].strip()
                        lastname = firstname_lastname[len(firstname) + 1:].strip()
                        member["key"] = ""
                        member["category"] = category.strip().lower()
                        member["firstname"] = firstname
                        member["lastname"] = lastname
                        member["moreinfos"] = moreinfos.strip()
                        if member not in team_members:
                            team_members.append(member)
                    position += 1
                extract_text = new_text[new_text.find(categories[position]):]
                extract_text = extract_text.replace(categories[position], "")
                result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
                for i in result:
                    member = dict()
                    category = categories[position]
                    for key in clear_categories_mappings.keys():
                        if category == clear_categories_mappings.get(key):
                            category = key
                            break
                    firstname_lastname = i.__getitem__(0).strip()
                    moreinfos = i.__getitem__(1)
                    tokens = firstname_lastname.split(' ')
                    firstname = tokens[0].strip()
                    lastname = firstname_lastname[len(firstname) + 1:].strip()
                    member["key"] = ""
                    member["category"] = " ".join(category.replace('\n', ' ').strip().lower().split())
                    member["firstname"] = firstname
                    member["lastname"] = lastname
                    member["moreinfos"] = moreinfos.strip()
                    if member not in team_members:
                        team_members.append(member)
    team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def get_1999_team_activities_raweb():
    teams = dict()
    year = 1999
    link = "http://raweb.inria.fr/rapportsactivite/RA1999/index.html"
    start_url = "http://raweb.inria.fr/rapportsactivite/RA1999/"
    response = requests.get(link)
    soup = BeautifulSoup(response.content, "html.parser")
    # print(soup.prettify())
    results = soup.findAll("li")
    if bool(results):
        list_teams = []
        for result in results:
            team = dict()
            acronym = result.a.text.strip().lower()
            url = result.a['href']
            if "html" in url:
                team = dict()
                print(result.a.text + " " + result.a['href'])
                team["acronym"] = acronym
                team["url"] = start_url + url
                team["year"] = year
                list_teams.append(team)
        teams[str(year)] = list_teams
    return teams


def get_1999_team_composition(link_report_activity, acronym, year):
    team_structure = dict()
    team_members = []
    categories = []
    part_of_link = ""
    team_compo_link_end = "compo_mn.html"
    text_to_find_1 = '''Composition de
          l'équipe'''
    text_to_find_2 = "Composition de l'équipe"
    team_structure["acronym"] = acronym.lower()
    team_structure["url"] = link_report_activity
    team_structure["year"] = year
    transform_link = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    tokens = transform_link.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"
    link_to_parse = new_link_start + team_compo_link_end
    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("a")
    if results:
        categories = []
        for result in results:
            cat = result.text.replace('\n', ' ').strip()
            cat = " ".join(cat.split())
            if cat != "" and cat not in categories:
                categories.append(" ".join(cat.split()))
        response2 = requests.get(link_to_parse)
        soup2 = BeautifulSoup(response2.content, "html.parser")

    results2 = soup2.findAll("frameset")
    if response2:
        for result2 in results2:
            text = result2.text.replace(text_to_find_1, "")
            text = text.replace(text_to_find_2, "")
            text = text.strip()
            new_text = " ".join(text.split())

            counter = 0
            categories_mappings = dict()
            for val1 in categories:
                for val2 in categories:
                    status = 0
                    if val1 != val2 and val1 in val2:
                        categories_mappings[val2] = "#" + str(counter) + "#"
                        break
                counter += 1

            keys = categories_mappings.keys()
            for i, n in enumerate(categories):
                if n in keys:
                    categories[i] = categories_mappings.get(n)
            clear_categories_mappings = dict()
            for val in categories:
                if "#" not in val:
                    clear_categories_mappings[val] = val
                else:
                    for key in keys:
                        if categories_mappings.get(key) == val:
                            clear_categories_mappings[key] = val

            for key in clear_categories_mappings.keys():
                if key in new_text:
                    replacement = clear_categories_mappings.get(key) + " "
                    new_text = new_text.replace(key, replacement)
            iter = len(categories) - 1
            position = 0

            while position < iter:

                begin = new_text.find(categories[position])
                end = new_text.find(categories[position + 1])

                extract_text = new_text[begin:end]

                extract_text = extract_text.replace(categories[position], "")
                result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
                for i in result:
                    member = dict()
                    category = categories[position]
                    for key in clear_categories_mappings.keys():
                        if category == clear_categories_mappings.get(key):
                            category = key
                            break
                    firstname_lastname = i.__getitem__(0).strip()
                    moreinfos = i.__getitem__(1)
                    tokens = firstname_lastname.split(' ')
                    firstname = tokens[0].strip()
                    lastname = firstname_lastname[len(firstname) + 1:].strip()
                    member["key"] = ""
                    member["category"] = category.strip().lower()
                    member["firstname"] = firstname
                    member["lastname"] = lastname
                    member["moreinfos"] = moreinfos.strip()
                    team_members.append(member)
                position += 1
            extract_text = new_text[new_text.find(categories[position]):]
            extract_text = extract_text.replace(categories[position], "")
            result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
            for i in result:
                member = dict()
                category = categories[position]
                for key in clear_categories_mappings.keys():
                    if category == clear_categories_mappings.get(key):
                        category = key
                        break
                firstname_lastname = i.__getitem__(0).strip()
                moreinfos = i.__getitem__(1)
                tokens = firstname_lastname.split(' ')
                firstname = tokens[0].strip()
                lastname = firstname_lastname[len(firstname) + 1:].strip()
                member["key"] = ""
                member["category"] = " ".join(category.replace('\n', ' ').strip().lower().split())
                member["firstname"] = firstname
                member["lastname"] = lastname
                member["moreinfos"] = moreinfos.strip()
                team_members.append(member)
    team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def get_1995_team_activities_raweb():
    teams = dict()
    list_teams = []
    year = 1995
    link = "http://raweb.inria.fr/rapportsactivite/RA1995/index.html"
    team_pages_start_url = "http://raweb.inria.fr/rapportsactivite/RA1995/"
    response = requests.get(link)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("a")
    if bool(results):
        for result in results:
            if "[" not in result.text:
                team = dict()
                team["acronym"] = result.text
                team["url"] = team_pages_start_url + result['href']
                print(team_pages_start_url + result['href'])
                team["year"] = str(year)
                list_teams.append(team)
        teams[str(year)] = list_teams
    return teams


def get_1995_team_composition(link_report_activity, acronym, year):
    print(link_report_activity)
    team_structure = dict()
    team_members = []
    categories = []
    link_to_find = "Composition de l'équipe"
    to_exclude = ["Précédent :", "Remonter :", "Suivant :"]
    url_start = "http://"
    end_url = ""
    new_link_start = ""
    transform_link = link_report_activity.replace("http://", "")
    tokens = transform_link.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val.strip() + "/"
    response = requests.get(link_report_activity)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("a")
    new_link_start = url_start + new_link_start
    if bool(results):
        for result in results:
            link_to_find = " ".join(link_to_find.split())
            check = " ".join(result.text.replace('\n', ' ').strip().split())
            if check == link_to_find:
                end_url = result['href']
                break

    if end_url != "":
        new_link_start = new_link_start + end_url
        response2 = requests.get(new_link_start)
        soup2 = BeautifulSoup(response2.content, "html.parser")
        results2 = soup2.findAll("b")
        if bool(results2):
            for result2 in results2:
                cat = " ".join(result2.text.replace('\n', ' ').strip().split())
                if cat not in to_exclude:
                    if cat != "" and cat not in categories:
                        categories.append(" ".join(cat.split()))

        response3 = requests.get(new_link_start)
        soup3 = BeautifulSoup(response3.content, "html.parser")
        results3 = soup3.findAll("body")
        if bool(results3):
            for result3 in results3:
                text = result3.text.strip()
                new_text = " ".join(text.split())
                counter = 0
                categories_mappings = dict()
                for val1 in categories:
                    for val2 in categories:
                        status = 0
                        if val1 != val2 and val1 in val2:
                            categories_mappings[val2] = "#" + str(counter) + "#"
                            break
                    counter += 1

                keys = categories_mappings.keys()
                for i, n in enumerate(categories):
                    if n in keys:
                        categories[i] = categories_mappings.get(n)
                clear_categories_mappings = dict()
                for val in categories:
                    if "#" not in val:
                        clear_categories_mappings[val] = val
                    else:
                        for key in keys:
                            if categories_mappings.get(key) == val:
                                clear_categories_mappings[key] = val

                for key in clear_categories_mappings.keys():
                    if key in new_text:
                        replacement = clear_categories_mappings.get(key) + " "
                        new_text = new_text.replace(key, replacement)
                iter = len(categories) - 1
                position = 0

                while position < iter:

                    begin = new_text.find(categories[position])
                    end = new_text.find(categories[position + 1])

                    extract_text = new_text[begin:end]

                    extract_text = extract_text.replace(categories[position], "")
                    result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
                    for i in result:
                        member = dict()
                        category = categories[position]
                        for key in clear_categories_mappings.keys():
                            if category == clear_categories_mappings.get(key):
                                category = key
                                break
                        firstname_lastname = i.__getitem__(0).strip()
                        moreinfos = i.__getitem__(1)
                        tokens = firstname_lastname.split(' ')
                        firstname = tokens[0].strip()
                        lastname = firstname_lastname[len(firstname) + 1:].strip()
                        member["key"] = ""
                        member["category"] = category.strip().lower()
                        member["firstname"] = firstname
                        member["lastname"] = lastname
                        member["moreinfos"] = moreinfos.strip()
                        team_members.append(member)
                    position += 1
                extract_text = new_text[new_text.find(categories[position]):]
                extract_text = extract_text.replace(categories[position], "")
                result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
                for i in result:
                    member = dict()
                    category = categories[position]
                    for key in clear_categories_mappings.keys():
                        if category == clear_categories_mappings.get(key):
                            category = key
                            break
                    firstname_lastname = i.__getitem__(0).strip()
                    moreinfos = i.__getitem__(1)
                    tokens = firstname_lastname.split(' ')
                    firstname = tokens[0].strip()
                    lastname = firstname_lastname[len(firstname) + 1:].strip()
                    member["key"] = ""
                    member["category"] = " ".join(category.replace('\n', ' ').strip().lower().split())
                    member["firstname"] = firstname
                    member["lastname"] = lastname
                    member["moreinfos"] = moreinfos.strip()
                    team_members.append(member)
    team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def get_2004_to_2007_team_activities_raweb():
    teams = dict()
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    # counter = 0
    for i in range(2004, 2008):
        list_teams = []
        link = star_url + str(i) + "/" + end_url
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        results = soup.findAll("a")
        if bool(results):
            start_iter = 4
            result_count = len(results)
            for j in range(4, result_count):
                result = results[j]
                team = dict()
                team["acronym"] = result.text
                team["url"] = star_url + str(i) + "/" + result['href']
                team["year"] = i
                # print(result.text + ": " + star_url + str(i) + "/" + result['href'])
                list_teams.append(team)
                teams[str(i)] = team
        teams[str(i)] = list_teams
    return teams


def get_2008_to_2009_team_activities_raweb():
    teams = dict()
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    # counter = 0
    for i in range(2008, 2010):
        list_teams = []
        link = star_url + str(i) + "/" + end_url
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        results = soup.findAll("a")
        if bool(results):
            start_iter = 4
            result_count = len(results)
            for j in range(4, result_count):
                result = results[j]
                team = dict()
                team["acronym"] = result.text
                team["url"] = star_url + str(i) + "/" + result['href']
                team["year"] = i
                list_teams.append(team)
        teams[str(i)] = list_teams
    return teams


def get_2004_to_2007_team_composition(link_report_activity, acronym, year):
    team_structure = dict()
    team_members = []
    team_structure["acronym"] = acronym.lower()
    team_structure["url"] = link_report_activity
    team_structure["year"] = year
    transform_link_report_activity = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    tokens = transform_link_report_activity.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"
    link_to_parse = new_link_start + str(acronym.strip().lower()) + ".xml"
    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    team = soup.find("team")
    results = team.findAll("participants")
    if bool(results):
        for result in results:
            if result.has_attr('category'):
                cat = result['category'].replace("_", " ").strip().lower()
                if cat == "None".lower():
                    category = ""
                else:
                    category = cat
            else:
                category = ""
            persons = result.findAll("person")
            for person in persons :
                member = dict()
                member["category"] = category

                if person.has_attr('key'):
                    key = person['key'].strip()
                    if key == "PASUSERID":
                        member["key"] = ""
                    else:
                        member["key"] = key
                else:
                    member["key"] = ""
                member["firstname"] = person.firstname.text.strip()
                member["lastname"] = person.lastname.text.strip()
                moreinfo = person.moreinfo
                if moreinfo:
                    member["moreinfos"] = person.moreinfo.text.strip()
                else:
                    member["moreinfos"] = ""
                team_members.append(member)

    team_structure["members"] = team_members
    return team_structure


def get_2008_to_2016_team_composition(link_report_activity, acronym, year):
    team_structure = dict()
    team_members = []
    team_structure["acronym"] = acronym.lower()
    team_structure["url"] = link_report_activity
    team_structure["year"] = year
    transform_link_report_activity = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    tokens = transform_link_report_activity.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"
    if " " in acronym.lower() :
        acronym = acronym.replace(" ", "_")
    if "é" in acronym.lower() :
        acronym = acronym.replace("é", "e")
    link_to_parse = new_link_start + str(acronym.strip().lower()) + ".xml"
    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    team = soup.find("team")
    results = team.findAll("person")
    if bool(results):
        for result in results:
            member = dict()

            if result.has_attr('key'):
                key = result['key'].strip()
                if key == "PASUSERID":
                    member["key"] = ""
                else:
                    member["key"] = key
            else:
                member["key"] = ""

            category = ""
            research_centre_text = ""
            affiliation_text = ""
            moreinfos = ""
            for child in result.findChildren():
                if child.name == 'categorypro':
                    category = child.text.strip().lower()
                if child.name == 'affiliation':
                    affiliation_text = child.text.strip()
                if child.name == 'research-centre':
                    research_centre_text = child.text.strip()
                if child.name == 'moreinfo':
                    moreinfos = child.text.strip()

            member["category"] = category
            member["firstname"] = result.firstname.text.strip()
            member["lastname"] = result.lastname.text.strip()
            member["moreinfos"] = moreinfos + " # " + affiliation_text + " # " + research_centre_text
            team_members.append(member)
    team_structure["members"] = team_members
    return team_structure


def get_2004_to_2009_team_report(link_report_activity, acronym):
    report_contains = ""
    transform_link_report_activity = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    tokens = transform_link_report_activity.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"
    link_to_parse = new_link_start + str(acronym.strip().lower()) + ".xml"
    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll(["presentation", "fondements", "domaine", "resultats"])
    if bool(results):
        for result in results:
            report_contains = report_contains + " " + result.text
    # print(report_contains)
    return report_contains


def get_2010_team_activities_raweb():
    teams = dict()
    list_teams_same_year = []
    year = 2010
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    link = star_url + str(year) + "/" + end_url
    response = requests.get(link)
    soup = BeautifulSoup(response.content, "html.parser")
    result = soup.find("ul")
    if result:
        children = result.findChildren()
        for child in children:
            if child.name == "a" and child.has_attr("href"):
                team = dict()
                team["acronym"] = child.text.strip().lower()
                team["url"] = star_url + str(year) + "/" + child['href']
                team["year"] = str(year)
                list_teams_same_year.append(team)
    teams[str(year)] = list_teams_same_year
    return teams


def get_2011_to_2016_team_activities_raweb():
    teams = dict()
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    for i in range(2011, 2017):

        list_teams_same_year = []
        link = star_url + str(i) + "/" + end_url
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        result = soup.find("div", {"id": "listeAlpha"})
        if result:
            children = result.findChildren()
            for child in children:
                if child.name == "a" and child.has_attr("href"):
                    team = dict()
                    team["acronym"] = child.text.strip().lower()
                    team["url"] = star_url + str(i) + "/" + child['href']
                    team["year"] = i
                    list_teams_same_year.append(team)
        teams[str(i)] = list_teams_same_year
    #print(teams.get("2012"))
    return teams


def get_2002_to_2003_team_activities_raweb():
    teams = dict()
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    for i in range(2002, 2004):
        list_teams_same_year = []
        link = star_url + str(i) + "/" + end_url
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        results = soup.findAll("a")
        if bool(results):
            result_count = len(results)
            for j in range(4, result_count):
                result = results[j]
                team = dict()
                team["acronym"] = result.text
                team["url"] = star_url + str(i) + "/" + result['href']
                team["year"] = i
                list_teams_same_year.append(team)
        teams[str(i)] = list_teams_same_year
    return teams


def get_2002_to_2003_team_composition(link_report_activity, acronym, year):
    team_structure = dict()
    team_members = []
    team_structure["acronym"] = acronym.lower()
    team_structure["url"] = link_report_activity
    team_structure["year"] = year
    transform_link = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    new_link_end = "uid1.html"
    tokens = transform_link.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"
    link_to_parse = new_link_start + new_link_end
    if link_to_parse == "http://raweb.inria.fr/rapportsactivite/RA2002/in-situ/uid1.html":
        link_to_parse = "http://raweb.inria.fr/rapportsactivite/RA2002/in-situ/section_composition.html"
    if link_to_parse == "http://raweb.inria.fr/rapportsactivite/RA2003/wam/uid1.html":
        link_to_parse = "http://raweb.inria.fr/rapportsactivite/RA2003/wam/id2592913.html"
    # print(link_to_parse)

    response = requests.get(link_to_parse)
    soup = BeautifulSoup(response.content, "html.parser")
    result = soup.find("div", {"id": "main"})
    if result:
        text = result.text.strip()
        categories = []
        children = result.findChildren()
        for child in children:
            if child.name == "h3":
                cat = child.text.replace('\n', ' ').strip()
                cat = " ".join(cat.split())
                if cat.strip() != "" and cat not in categories:
                    categories.append(cat)
        new_text = ""
        for word in text.split():
            if word.strip().lower() == "team":
                word = ""
            if word.strip().lower() == "logo":
                break
            new_text = new_text + word.strip() + " "
        counter = 0
        categories_mappings = dict()
        for val1 in categories:
            for val2 in categories:
                status = 0
                if val1 != val2 and val1 in val2:
                    categories_mappings[val2] = "#" + str(counter) + "#"
                    break
            counter += 1

        keys = categories_mappings.keys()
        for i, n in enumerate(categories):
            if n in keys:
                categories[i] = categories_mappings.get(n)
        clear_categories_mappings = dict()
        for val in categories:
            if "#" not in val:
                clear_categories_mappings[val] = val
            else:
                for key in keys:
                    if categories_mappings.get(key) == val:
                        clear_categories_mappings[key] = val

        for key in clear_categories_mappings.keys():
            if key in new_text:
                replacement = clear_categories_mappings.get(key) + " "
                new_text = new_text.replace(key, replacement)
        iter = len(categories) - 1
        position = 0

        while position < iter:

            begin = new_text.find(categories[position])
            end = new_text.find(categories[position + 1])

            extract_text = new_text[begin:end]

            extract_text = extract_text.replace(categories[position], "")
            result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
            for i in result:
                member = dict()
                category = categories[position]
                for key in clear_categories_mappings.keys():
                    if category == clear_categories_mappings.get(key):
                        category = key
                        break

                firstname_lastname = i.__getitem__(0).strip()
                moreinfos = i.__getitem__(1)
                tokens = firstname_lastname.split(' ')
                firstname = tokens[0].strip()
                lastname = firstname_lastname[len(firstname) + 1:].strip()
                member["key"] = ""
                member["category"] = category.strip().lower()
                member["firstname"] = firstname
                member["lastname"] = lastname
                member["moreinfos"] = moreinfos.strip()
                if member not in team_members:
                    team_members.append(member)
            position += 1
        extract_text = new_text[new_text.find(categories[position]):]
        extract_text = extract_text.replace(categories[position], "")
        result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
        for i in result:
            member = dict()
            category = categories[position]
            for key in clear_categories_mappings.keys():
                if category == clear_categories_mappings.get(key):
                    category = key
                    break
            firstname_lastname = i.__getitem__(0).strip()
            moreinfos = i.__getitem__(1)
            tokens = firstname_lastname.split(' ')
            firstname = tokens[0].strip()
            lastname = firstname_lastname[len(firstname) + 1:].strip()
            member["key"] = ""
            member["category"] = " ".join(category.replace('\n', ' ').strip().lower().split())
            member["firstname"] = firstname
            member["lastname"] = lastname
            member["moreinfos"] = moreinfos.strip()
            if member not in team_members:
                team_members.append(member)
    team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def get_2000_to_2001_team_activities_raweb():
    teams = dict()
    star_url = "http://raweb.inria.fr/rapportsactivite/RA"
    end_url = "index.html"
    # counter = 0
    for i in range(2000, 2002):
        list_teams = []
        link = star_url + str(i) + "/" + end_url
        response = requests.get(link)
        soup = BeautifulSoup(response.content, "html.parser")
        results = soup.findAll("strong")
        if bool(results):
            for result in results:
                team = dict()
                team["acronym"] = result.a.text
                team["url"] = star_url + str(i) + "/" + result.a['href']
                team["year"] = i
                # print(result.a.text + ": " + star_url + str(i) + "/" + result.a['href'])
                list_teams.append(team)
                teams[str(i)] = team
        teams[str(i)] = list_teams
    return teams


def get_2000_to_2001_team_composition(link_report_activity, acronym, year):
    categories = []
    team_structure = dict()
    team_members = []
    part_of_link = ""
    text_to_find = '''Composition de
          l'équipe'''
    team_structure["acronym"] = acronym.lower()
    team_structure["url"] = link_report_activity
    team_structure["year"] = year
    transform_link = link_report_activity.replace("http://", "")
    new_link_start = "http://"
    tokens = transform_link.split("/")
    for val in tokens:
        if "html" not in val:
            new_link_start = new_link_start + val + "/"
    response = requests.get(link_report_activity)
    soup = BeautifulSoup(response.content, "html.parser")
    results = soup.findAll("a")
    for result in results:
        # print(result.text)
        if result.text == text_to_find:
            part_of_link = result['href']
    if part_of_link != "":
        link_to_parse = new_link_start + part_of_link
        response3 = requests.get(link_to_parse)

    soup3 = BeautifulSoup(response3.content, "html.parser")
    results3 = soup3.findAll("a")
    if results3:
        categories = []
        for result3 in results3:
            cat = result3.text.replace('\n', ' ').strip()
            cat = " ".join(cat.split())
            if cat != "" and cat not in categories:
                categories.append(cat)
        print("cat: " + str(categories))
    response2 = requests.get(link_to_parse)
    soup2 = BeautifulSoup(response2.content, "html.parser")
    results2 = soup2.findAll("frameset")
    if results2:
        for result2 in results2:
            # print(result2.text)
            text = result2.text.replace('\n', ' ').strip()
            text = " ".join(text.split())
            # print(text)
            to_replace = "Composition de l'équipe"
            delete_text_from = "Sous-sections"
            new_text = text.replace(to_replace, "")
            begin = new_text.find(delete_text_from)
            new_text_clean = new_text.replace(new_text[begin:], "").strip()
            # print(new_text_clean)
            counter = 0
            categories_mappings = dict()
            for val1 in categories:
                for val2 in categories:
                    status = 0
                    if val1 != val2 and val1 in val2:
                        categories_mappings[val2] = "#" + str(counter) + "#"
                        break
                    counter += 1

            keys = categories_mappings.keys()
            for i, n in enumerate(categories):
                if n in keys:
                    categories[i] = categories_mappings.get(n)
            clear_categories_mappings = dict()
            for val in categories:
                if "#" not in val:
                    clear_categories_mappings[val] = val
                else:
                    for key in keys:
                        if categories_mappings.get(key) == val:
                            clear_categories_mappings[key] = val

            for key in clear_categories_mappings.keys():
                if key in new_text:
                    replacement = clear_categories_mappings.get(key) + " "
                    new_text = new_text.replace(key, replacement)

        iter = len(categories) - 1
        position = 0
        # print(new_text_clean_2)
        print(categories)
        while position < iter:

            begin = new_text.find(categories[position])
            end = new_text.find(categories[position + 1])

            extract_text = new_text[begin:end]

            extract_text = extract_text.replace(categories[position], "")
            res = re.findall(r'(.*?)\[(.*?)\]', extract_text)
            for i in res:
                member = dict()
                category = categories[position]
                for key in clear_categories_mappings.keys():
                    if category == clear_categories_mappings.get(key):
                        category = key
                        break
                firstname_lastname = i.__getitem__(0).strip()
                moreinfos = i.__getitem__(1)
                tokens = firstname_lastname.split(' ')
                firstname = tokens[0].strip()
                lastname = firstname_lastname[len(firstname) + 1:].strip()
                member["key"] = ""
                member["category"] = category.strip().lower()
                member["firstname"] = firstname
                member["lastname"] = lastname
                member["moreinfos"] = moreinfos.strip()
                team_members.append(member)
            position += 1
        extract_text = new_text[new_text.find(categories[position]):]
        extract_text = extract_text.replace(categories[position], "")
        result = re.findall(r'(.*?)\[(.*?)\]', extract_text)
        for i in result:
            member = dict()
            category = categories[position]
            for key in clear_categories_mappings.keys():
                if category == clear_categories_mappings.get(key):
                    category = key
                    break
            firstname_lastname = i.__getitem__(0).strip()
            moreinfos = i.__getitem__(1)
            tokens = firstname_lastname.split(' ')
            firstname = tokens[0].strip()
            lastname = firstname_lastname[len(firstname) + 1:].strip()
            member["key"] = ""
            member["category"] = " ".join(category.replace('\n', ' ').strip().lower().split())
            member["firstname"] = firstname
            member["lastname"] = lastname
            member["moreinfos"] = moreinfos.strip()
            team_members.append(member)
    team_structure["members"] = team_members
    print(team_structure)
    return team_structure


def store_1994_in_db():
    teams = get_1994_team_activities_raweb().get("1994")
    for team in teams:
        team_to_store = dict()
        acronym = team["acronym"]
        url = team["url"]
        year = team["year"]
        team_to_store["acronym"] = acronym.strip().lower()
        team_to_store["url"] = url
        team_to_store["year"] = int(year)
        team_to_store["members"] = get_1994_team_composition(url, acronym, year).get("members")
        data_to_mongodb(team_to_store)


def store_1995_in_db():
    teams = get_1995_team_activities_raweb().get("1995")
    for team in teams:
        team_to_store = dict()
        acronym = team["acronym"]
        url = team["url"]
        year = team["year"]
        team_to_store["acronym"] = acronym.strip().lower()
        team_to_store["url"] = url
        team_to_store["year"] = int(year)
        team_to_store["members"] = get_1995_team_composition(url, acronym, year).get("members")
        data_to_mongodb(team_to_store)


def store_1996_to_1998_in_db():
    for i in range(1996, 1999):
        teams = get_1996_to_1998_team_activities_raweb().get(str(i))
        for team in teams:
            team_to_store = dict()
            acronym = team["acronym"]
            url = team["url"]
            year = team["year"]
            team_to_store["acronym"] = acronym.strip().lower()
            team_to_store["url"] = url
            team_to_store["year"] = int(year)
            team_to_store["members"] = get_1996_to_1998_team_composition(url, acronym, year).get("members")
            data_to_mongodb(team_to_store)


def store_1999_in_db():
    teams = get_1999_team_activities_raweb().get("1999")
    for team in teams:
        team_to_store = dict()
        acronym = team["acronym"]
        url = team["url"]
        year = team["year"]
        team_to_store["acronym"] = acronym.strip().lower()
        team_to_store["url"] = url
        team_to_store["year"] = int(year)
        team_to_store["members"] = get_1999_team_composition(url, acronym, year).get("members")


def store_2000_to_2001_in_db():
    for i in range(2000, 2002):
        teams = get_2000_to_2001_team_activities_raweb().get(str(i))
        for team in teams:
            team_to_store = dict()
            acronym = team["acronym"]
            url = team["url"]
            year = team["year"]
            team_to_store["acronym"] = acronym.strip().lower()
            team_to_store["url"] = url
            team_to_store["year"] = int(year)
            team_to_store["members"] = get_2000_to_2001_team_composition(url, acronym, year).get("members")
            data_to_mongodb(team_to_store)


def store_2002_to_2003_in_db():
    for i in range(2002, 2004):
        teams = get_2002_to_2003_team_activities_raweb().get(str(i))
        for team in teams:
            team_to_store = dict()
            acronym = team["acronym"]
            url = team["url"]
            year = team["year"]
            team_to_store["acronym"] = acronym.strip().lower()
            team_to_store["url"] = url
            team_to_store["year"] = int(year)
            team_to_store["members"] = get_2002_to_2003_team_composition(url, acronym, year).get("members")
            data_to_mongodb(team_to_store)


def store_2004_to_2007_in_db():
    for i in range(2004, 2008):
        teams = get_2004_to_2007_team_activities_raweb().get(str(i))
        for team in teams:
            team_to_store = dict()
            acronym = team["acronym"]
            url = team["url"]
            year = team["year"]
            team_to_store["acronym"] = acronym.strip().lower()
            team_to_store["url"] = url
            team_to_store["year"] = int(year)
            team_to_store["members"] = get_2004_to_2007_team_composition(url, acronym, year).get("members")
            data_to_mongodb(team_to_store)


def store_2008_to_2009_in_db():
    for i in range(2008, 2010):
        teams = get_2008_to_2009_team_activities_raweb().get(str(i))
        for team in teams:
            team_to_store = dict()
            acronym = team["acronym"]
            url = team["url"]
            year = team["year"]
            team_to_store["acronym"] = acronym.strip().lower()
            team_to_store["url"] = url
            team_to_store["year"] = int(year)
            team_to_store["members"] = get_2008_to_2016_team_composition(url, acronym, year).get("members")
            data_to_mongodb(team_to_store)


def store_2010_in_db():
    teams = get_2010_team_activities_raweb().get("2010")
    for team in teams:
        team_to_store = dict()
        acronym = team["acronym"]
        url = team["url"]
        year = team["year"]
        team_to_store["acronym"] = acronym.strip().lower()
        team_to_store["url"] = url
        team_to_store["year"] = int(year)
        team_to_store["members"] = get_2008_to_2016_team_composition(url, acronym, year).get("members")


def store_2011_to_2016_in_db():
    for i in range(2011, 2017):
        teams = get_2011_to_2016_team_activities_raweb().get(str(i))
        for team in teams:
            team_to_store = dict()
            acronym = team["acronym"]
            url = team["url"]
            year = team["year"]
            team_to_store["acronym"] = acronym.strip().lower()
            team_to_store["url"] = url
            team_to_store["year"] = int(year)
            team_to_store["members"] = get_2008_to_2016_team_composition(url, acronym, year).get("members")
            data_to_mongodb(team_to_store)


if __name__ == '__main__':

    database_init()
    store_1994_in_db()
    store_1995_in_db()
    store_1996_to_1998_in_db()
    store_1999_in_db()
    store_2000_to_2001_in_db()
    store_2002_to_2003_in_db()
    store_2002_to_2003_in_db()
    store_2004_to_2007_in_db()
    store_2008_to_2009_in_db()
    store_2010_in_db()
    store_2011_to_2016_in_db()
